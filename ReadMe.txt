the source of this summary
https://laravel-news.com/your-first-laravel-application

1) make new project
2) make login and regestration from beginning to aviod problems later
php artisan make:auth
3)make new migration then add title , url and description colunms then migrate it
php a

php artisan make:migration create_links_table --create=links

Schema::create('links', function (Blueprint $table) {
      $table->increments('id');
      $table->string('title');
      $table->string('url')->unique();
      $table->text('description');
      $table->timestamps();
});

php artisan migrate

4)make model and factory to build millions of facked data for testing then go to factory file and apply your data

php artisan make:model --factory Link

database/factories/LinkFactory.php

<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Link::class, function (Faker $faker) {
    return [
        'title' => substr($faker->sentence(2), 0, -1),
        'url' => $faker->url,
        'description' => $faker->paragraph,
    ];
});


5)make new seed then add your facotry inside it by the number of data you want then call the function from main then migrate and seed to create_links_table

php artisan make:seeder LinksTableSeeder

database/seeds/LinksTableSeeder.php

public function run()
{
    factory(App\Link::class, 5)->create();
}

database/seeds/DatabaseSeeder.php

public function run()
{
    $this->call(LinksTableSeeder::class);
}

$ php artisan migrate:fresh --seed

6)do your view and controller to show your Database
7)now it is time for testing
8)change database connection in phpunit.xml file then romve ExampleTest.php file then make new test filter

add to phpunit

<php>
        <!-- ... -->
    <env name="DB_CONNECTION" value="sqlite"/>
    <env name="DB_DATABASE" value=":memory:"/>
        <!-- ... -->
</php>


rm tests/Feature/ExampleTest.php


php artisan make:test SubmitLinksTest

9)inside tests/Feature/SubmitLinksTest.php make functions that has your testing conditions so the file will be as next

<?php

namespace Tests\Feature;

use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubmitLinksTest extends TestCase
{
    use RefreshDatabase;
    function guest_can_submit_a_new_link() {}
    function link_is_not_created_if_validation_fails() {}
    function link_is_not_created_with_an_invalid_url() {}
    function max_length_fails_when_too_long() {}
    function max_length_succeeds_when_under_max() {}

}


7)inside every function it will be your condition so the conditions will be as next

**Verify that valid links get saved in the database
function guest_can_submit_a_new_link()
   {
       $response = $this->post('/submit', [
           'title' => 'Example Title',
           'url' => 'http://example.com',
           'description' => 'Example description.',
       ]);

       $this->assertDatabaseHas('links', [
           'title' => 'Example Title'
       ]);

       $response
           ->assertStatus(302)
           ->assertHeader('Location', url('/'));

       $this
           ->get('/')
           ->assertSee('Example Title');
   }


**When validation fails, links are not in the database
function link_is_not_created_if_validation_fails()
{
    $response = $this->post('/submit');

    $response->assertSessionHasErrors(['title', 'url', 'description']);
}


**Invalid URLs are not allowed
function link_is_not_created_with_an_invalid_url()
{
    $this->withoutExceptionHandling();

    $cases = ['//invalid-url.com', '/invalid-url', 'foo.com'];

    foreach ($cases as $case) {
        try {
            $response = $this->post('/submit', [
                'title' => 'Example Title',
                'url' => $case,
                'description' => 'Example description',
            ]);
        } catch (ValidationException $e) {
            $this->assertEquals(
                'The url format is invalid.',
                $e->validator->errors()->first('url')
            );
            continue;
        }

        $this->fail("The URL $case passed validation when it should have failed.");
    }
}


**Validation should fail when the fields are longer than the max:255 validation
function max_length_fails_when_too_long()
{
    $this->withoutExceptionHandling();

    $title = str_repeat('a', 256);
    $description = str_repeat('a', 256);
    $url = 'http://';
    $url .= str_repeat('a', 256 - strlen($url));

    try {
        $this->post('/submit', compact('title', 'url', 'description'));
    } catch(ValidationException $e) {
        $this->assertEquals(
            'The title may not be greater than 255 characters.',
            $e->validator->errors()->first('title')
        );

        $this->assertEquals(
            'The url may not be greater than 255 characters.',
            $e->validator->errors()->first('url')
        );

        $this->assertEquals(
            'The description may not be greater than 255 characters.',
            $e->validator->errors()->first('description')
        );

        return;
    }

    $this->fail('Max length should trigger a ValidationException');
}


**Validation should succeed when the fields are long enough according to max:255.
function max_length_succeeds_when_under_max()
{
    $url = 'http://';
    $url .= str_repeat('a', 255 - strlen($url));

    $data = [
        'title' => str_repeat('a', 255),
        'url' => $url,
        'description' => str_repeat('a', 255),
    ];

    $this->post('/submit', $data);

    $this->assertDatabaseHas('links', $data);
}


10)Run the test suite and make sure everything is passing:

$ vendor/bin/phpunit
