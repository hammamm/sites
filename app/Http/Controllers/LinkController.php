<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;

class LinkController extends Controller
{
    public function index()
    {
      $links = Link::all();

   return view('welcome', ['links' => $links]);
    }


    public function edit()
    {
      return view('submit');

    }


    public function submit(Request $request)
    {
      // $client = new Guzzle\Http\Client(['base_uri' => 'http://note.test/api/']);
      // $response = $client->createRequest('POST', 'notes',[],['title'=>'test','body'=>'ttt']);
      // $client = new \Guzzle\Service\Client('http://note.test/api/notes/');
      // $response = $client->post()->send();
      $myBody = [];
      $client = new \GuzzleHttp\Client();

      $url = "http://note.test/api/notes/";


      $myBody['title'] = $request->title;
      $myBody['body'] = $request->body;


      $r = $client->post($url,[],  $myBody);

      $response = $r->send();
      return redirect('/');

    }


}
